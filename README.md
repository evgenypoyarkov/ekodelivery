# EKO Delivery Service

This is a (micro)service that offers set of APIs to solve some path tracing problems according to specification.

### Install


```
yarn
```

Via docker:


```
yarn build
```

### Usage

Show available scripts:


```
yarn describe
```

Run tests:


```
yarn test
```

### Notes

The implementation of path finding assumes that it will run within a standalone service: it uses slightly modified DFS to traverse the graph, 
then it creates several indexes to found routes between edges to speed-up queries.
