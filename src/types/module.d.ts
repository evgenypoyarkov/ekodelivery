declare module 'fastify-sensible'
declare module 'fastify-register-routes'
declare module 'fastify-graceful-shutdown'
declare module 'ms-conf'

type AnyFn = (...args: readonly any[]) => any

type DeepPartial<T> = {
  [P in keyof T]?: T[P] extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T[P] extends ReadonlyArray<infer X>
    ? ReadonlyArray<DeepPartial<X>>
    : DeepPartial<T[P]>
}

interface JSONSchemaResponse<Data = any, Meta = any> {
  data?: Data
  meta?: Meta
}

declare module 'fastify-cors' {
  import { HTTPMethod, Plugin } from 'fastify'
  export = FastifyCors

  const FastifyCors: FastifyCors.Instance

  namespace FastifyCors {
    type Instance = Plugin<any, any, any, any>

    interface Options {
      origin?: boolean | string | RegExp | Array<string | RegExp>
      methods?: HTTPMethod[]
      allowedHeaders?: string | string[]
      exposedHeaders?: string | string[]
      credentials?: boolean
      maxAge?: number
      preflightContinue?: boolean
      optionsSuccessStatus?: number
      preflight?: boolean
      hideOptionsRoute?: boolean
    }
  }
}
