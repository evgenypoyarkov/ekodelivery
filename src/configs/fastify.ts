import qs = require('qs')
import { ServerOptions } from 'fastify'

const kParseOptions: qs.IParseOptions = {
  plainObjects: true,
  allowDots: true,
}

export const fastify: ServerOptions = {
  logger: true,
  querystringParser: (str: string) => qs.parse(str, kParseOptions)
}
