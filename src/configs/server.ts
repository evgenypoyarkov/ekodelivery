import FastifyCors from 'fastify-cors'
import { ListenOptions } from 'fastify'

export const server: ListenOptions = {
  port: 3000,
  host: 'localhost',
}

export const cors: FastifyCors.Options = {
  origin: [
    /^.*localhost:\d+$/, //
    'https://demo.poyarkov.me',
  ],
}
