// tslint:disable-next-line:no-implicit-dependencies
import fp = require('fastify-plugin')

import { FastifyInstance } from 'fastify'
import { PathFinder, PathFinderConfig } from './path-finder'

function load(instance: FastifyInstance, _: PathFinderConfig, done: any): void {
  instance.decorate('finder', new PathFinder())
  instance.addSchema({
    $id: 'path-finder.common.json',
    definitions: {
      leg: {
        $id: '#leg',
        type: 'object',
        properties: {
          start: {
            type: 'string',
          },
          end: {
            type: 'string',
          },
          distance: {
            type: 'number'
          },
        },
      },
      route: {
        $id: '#route',
        type: 'object',
        properties: {
          cost: {
            type: 'number',
          },
          path: {
            type: 'array',
            items: {
              $ref: '#leg',
            },
          },
        },
      },
    },
  })
  done()
}

export = fp(load, {
  name: 'path-finder',
  fastify: '>=2.0.0',
})
