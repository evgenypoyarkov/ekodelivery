export const kNotFound = new Error('not-found')
export const kBadRequest = new Error('bad-request')
export const kMaxVisits = 2
