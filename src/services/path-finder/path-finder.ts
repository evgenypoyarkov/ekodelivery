import createGraph, { Graph, Node, Link } from 'ngraph.graph'
import { assert } from '../../utils/assert'
import { kBadRequest, kNotFound, kMaxVisits } from './constants'

export type CityConnections = Map<CityId, CityConnection>
export type Leg = Link<LinkData>
export type City = Node<NodeData>
export type CityId = City['id']
export type WorldGraph = Graph<NodeData, LinkData>

interface DFSStack {
  current: City
  path: Leg[]
  visits: Map<string, number>
  cost: number
  end: City
  hasLoops: boolean
}

export interface CityConnection {
  routes: Map<string, Route<Leg>>
  sorted: string[]
}

export interface NodeData {
  connections: CityConnections
  [k: string]: any
}

export interface LinkData {
  distance: number
}

export interface Road extends LinkData {
  start: CityId,
  end: CityId,
}

export interface Route<P = Road> {
  path: P[]
  cost: number
  hasLoops?: boolean
}

export interface RouteFilters {
  useTwice: boolean
  maxStops: number
  maxLength: number
}

export interface PathFinderService {
  cost(route: CityId[]): Route | false
  count(start: CityId, end: CityId, conditions: Partial<RouteFilters>): number
  cheapest(start: CityId, end: CityId): Route

  add(links: Road[]): number
  list(): Road[]
}

// tslint:disable-next-line:no-empty-interface
export interface PathFinderConfig {}

export class PathFinder implements PathFinderService {
  private static kSearchDefaults: Partial<RouteFilters> = {
    useTwice: false,
  }

  private static initCityConnection = (source: City, goal: City): CityConnection => {
    const connection = source.data.connections.get(goal.id) ?? {
      routes: new Map(),
      sorted: [],
    }

    if (source.data.connections.has(goal.id) === false) {
      source.data.connections.set(goal.id, connection)
    }

    return connection
  }

  private graph: WorldGraph = createGraph()
  private index: Map<CityId, Route> = new Map()

  public add(roads: Road[]): number {
    const added = []
    // pause updates in case if any subscriber would be attached
    this.graph.beginUpdate()

    for (const road of roads) {
      const { start, end, distance } = road

      /** attempt to initialize nodes */
      this.initNode(start)
      this.initNode(end)

      const link = this.initLink(start, end, { distance })

      if (link) {
        added.push(link)
      }
    }

    this.reindex()
    // release
    this.graph.endUpdate()
    return added.length
  }

  public list(): Road[] {
    const roads: Road[] = []

    this.graph.forEachLink(link => {
      roads.push({
        start: link.fromId,
        end: link.toId,
        distance: link.data.distance
      })
    })

    return roads
  }

  public cost(path: CityId[] = []): Route {
    if (path.length < 2) {
      throw kBadRequest
    }

    const index = path.join('')
    const route = this.index.get(index)

    if (!route) {
      throw kNotFound
    }

    return {
      cost: route.cost,
      path: this.restorePath(index),
    }
  }

  public count(startId?: CityId, endId?: CityId, conditions: Partial<RouteFilters> = {}): number {
    const filters = {
      ...PathFinder.kSearchDefaults,
      ...conditions,
    }

    const connection = this.getConnection(startId, endId)
    const matches = connection.sorted.filter((idx: string) => {
      // Do not count start node
      if (filters.maxStops !== undefined && idx.length - 1 > filters.maxStops) {
        return false
      }

      const route = connection.routes.get(idx) as Route<Leg>
      if (filters.useTwice === false && route.hasLoops) {
        return false
      }

      const length = route.cost ?? Number.POSITIVE_INFINITY
      if (filters.maxLength !== undefined && filters.maxLength < length) {
        return false
      }

      return true
    })

    return matches.length
  }

  public cheapest(startId?: CityId, endId?: CityId): Route {
    const connections = this.getConnection(startId, endId)
    const minRoute = connections.sorted[0]
    const { cost } = this.index.get(minRoute) as Route
    return {
      cost,
      path: this.restorePath(minRoute)
    }
  }

  private initNode(id: CityId): City | null {
    return this.graph.addNode(id, {
      connections: new Map()
    })
  }

  private initLink(start: CityId, end: CityId, data: LinkData): Leg | null {
    const existingLink = this.graph.getLink(start, end)

    // perform update
    if (existingLink) {
      existingLink.data = data
      return null
    }

    return this.graph.addLink(start, end, data)
  }

  private reindex = (): void => {
    const start = process.hrtime()
    const connections = new Map()

    // iterate over each node of the graph
    this.graph.forEachNode((node: City) => {
      node.data.connections = new Map()

      // initialize connections to all other destinations
      this.graph.forEachNode((goal: City) => {
        const connection = PathFinder.initCityConnection(node, goal)

        // walk down the graph using dfs
        this.walk(node, goal)

        // sort routes by their total distance and put all of them into the "global" index to speed up searches by route milestones (e.g. case #1)
        for (const [index, route] of connection.routes.entries()) {
          connection.sorted = Array
            .from(connection.routes.entries())
            .sort((a, b) => a[1].cost - b[1].cost)
            .map(it => it[0])
          connections.set(index, route)
        }
      })
    })

    const total = process.hrtime(start)
    const totalMs = (total[0] * 1e9 + total[1]) / 1e6

    // tslint:disable-next-line:no-console
    console.log('connections:', totalMs, connections.size)

    this.index = connections
  }

  private walk(source: City, end: City): void {
    const stack: DFSStack[] = [{
      end,
      cost: 0,
      path: [],
      current: source,
      visits: new Map(),
      hasLoops: false
    }]

    while (stack.length > 0) {
      const { current, path, visits, cost, end: goal, hasLoops } = stack.pop() as DFSStack
      const connection = PathFinder.initCityConnection(source, goal)

      this.graph.forEachLinkedNode(current.id, (next: City, link: Leg) => {
        const inclusions = visits.get(link.id) ?? 0
        const nextInclusions = inclusions + 1
        const nextPath = path.concat(link)
        const nextCost = cost + link.data.distance
        const nextHasLoops = hasLoops || nextInclusions === kMaxVisits

        visits.set(link.id, nextInclusions)

        // Path is found!
        // The eligible path is that finished in the goal node and has at least 1 span
        if (next.id === goal.id && nextPath.length > 0) {
          const index = nextPath.reduce((str: string[], it: Leg, idx: number) => {
            if (idx === 0) {
              return str.concat(it.fromId as string, it.toId as string)
            }

            return str.concat(it.toId as string)
          }, [])

          connection.routes?.set(index.join(''), {
            cost: nextCost,
            path: nextPath,
            hasLoops: nextHasLoops,
          })
        }

        // If this route is still could be reused, e.g. has less visits than we want,
        // push the task back to the stack
        if (inclusions < kMaxVisits) {
          stack.push({
            end: goal,
            current: next,
            path: nextPath,
            cost: nextCost,
            visits: new Map([...visits]),
            hasLoops: nextHasLoops
          })
        }
      }, true)
    }
  }

  private getConnection(startId?: CityId, endId?: CityId): CityConnection {
    assert(typeof startId !== 'undefined')
    assert(typeof endId !== 'undefined')

    const beginning = this.graph.getNode(startId)
    if (!beginning) {
      throw kNotFound
    }

    const connections = beginning.data.connections.get(endId)
    if (!connections?.sorted?.length) {
      throw kNotFound
    }

    return connections
  }

  private restorePath(route: string): Road[] {
    const road: Road[] = []

    for (let startIdx = 0, endIdx = startIdx + 1; endIdx <= route.length - 1; startIdx++, endIdx++) {
      const start = route[startIdx]
      const end = route[endIdx]
      const idx = `${start}${end}`
      const { cost } = this.index.get(idx) as Route

      road.push({
        start,
        end,
        distance: cost
      })
    }

    return road
  }
}
