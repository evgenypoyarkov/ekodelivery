import conf = require('ms-conf')
import path = require('path')

conf.prependDefaultConfiguration(path.resolve(__dirname, './configs'))

export { conf }
