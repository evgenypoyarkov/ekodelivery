import * as fastify from 'fastify'

export type ShutdownHandler = (signal: string, next: AnyFn) => Promise<void>
export type GracefulFastify = fastify.FastifyInstance & {
  gracefulShutdown(handler: ShutdownHandler): void
}

export function onShutdown(err: Error, context: fastify.FastifyInstance, next: any): void {
  const instance = context as GracefulFastify

  if (err instanceof Error) {
    // tslint:disable-next-line:no-console
    console.error(err)
    process.exit(1)
  }

  if (typeof instance.gracefulShutdown !== 'function') {
    return
  }

  const handler = async (_: string, done: AnyFn) => {
    // tslint:disable-next-line:no-console
    console.log('closing gracefully')
    // tslint:disable-next-line:no-console
    await instance.close()
    done()
  }

  instance.gracefulShutdown(handler)
  next()
}
