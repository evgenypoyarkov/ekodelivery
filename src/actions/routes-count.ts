import * as http from 'http'
import { FastifyInstance, FastifyRequest, RouteOptions, RouteSchema } from 'fastify'
import { Service } from '../index'
import { CityId, RouteFilters } from '../services/path-finder/path-finder'

interface RoutesCountRequestQuery {
  start?: CityId
  end?: CityId
  conditions?: Partial<RouteFilters>
}

interface RoutesCountResponseData {
  count: number
}

type RoutesCountRequest = FastifyRequest<
  http.IncomingMessage,
  RoutesCountRequestQuery,
  unknown,
  unknown,
  unknown
  >

const schema: RouteSchema = {
  querystring: {
    type: 'object',
    required: [
      'start', //
      'end',
      'conditions',
    ],
    properties: {
      start: {
        type: 'string',
        minLength: 1,
      },
      end: {
        type: 'string',
        minLength: 1,
      },
      conditions: {
        type: 'object',
        properties: {
          useTwice: {
            type: 'boolean',
            default: false
          },
          maxStops: {
            type: 'number',
            min: 1
          },
          maxLength: {
            type: 'number',
            min: 1
          }
        }
      }
    },
  },
  response: {
    '2xx': {
      type: 'object',
      required: ['data'],
      properties: {
        data: {
          type: 'object',
          required: ['count'],
          properties: {
            count: {
              type: 'number'
            }
          }
        }
      }
    },
  },
}

async function handler(this: FastifyInstance, request: RoutesCountRequest): Promise<JSONSchemaResponse<RoutesCountResponseData>> {
  const { query } = request

  try {
    const count = (this as Service).finder.count(query?.start, query?.end, query.conditions)
    return {
      data: {
        count
      }
    }
  } catch (err) {
    throw this.httpErrors.notFound('No Such Route')
  }
}

const routesCount: RouteOptions = {
  handler,
  schema,
  method: 'GET',
  url: '/routes/count',
}

export = routesCount
