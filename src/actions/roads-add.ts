import * as http from 'http'
import { FastifyInstance, FastifyRequest, RouteOptions, RouteSchema } from 'fastify'
import { Service } from '../index'
import { Road } from '../services/path-finder/path-finder'

interface AddRoadRequestBody {
  roads: string[]
}

interface AddRoadResponseData {
  added: number
}

type RoadsAddRequest = FastifyRequest<
  http.IncomingMessage,
  unknown,
  unknown,
  unknown,
  AddRoadRequestBody
>

const schema: RouteSchema = {
  body: {
    type: 'object',
    required: [
      'roads', //
    ],
    properties: {
      roads: {
        type: 'array',
        items: {
          $ref: '#/definitions/road',
        },
        minItems: 1,
        uniqueItems: true,
      }
    },
    definitions: {
      road: {
        type: 'string',
        pattern: '^[A-Za-z]{2}[0-9]+?$',
      }
    }
  },
  response: {
    '2xx': {
      type: 'object',
      required: ['data'],
      properties: {
        data: {
          type: 'object',
          properties: {
            added: {
              type: 'number',
            }
          }
        }
      }
    },
  },
}

const mapProvidedRoads = (it: string): Road => {
  const start = it[0]
  const end = it[1]
  const distance = parseInt(it.slice(2), 10)
  return {
    start,
    end,
    distance,
  }
}

async function handler(this: FastifyInstance, request: RoadsAddRequest): Promise<JSONSchemaResponse<AddRoadResponseData>> {
  const roads = request.body.roads.map(mapProvidedRoads)
  const added = (this as Service).finder.add(roads)

  return {
    data: {
      added,
    }
  }
}

const roadsAdd: RouteOptions = {
  handler,
  schema,
  method: 'POST',
  url: '/roads/add',
}

export = roadsAdd
