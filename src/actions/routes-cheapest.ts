import * as http from 'http'
import { FastifyInstance, FastifyRequest, RouteOptions, RouteSchema } from 'fastify'
import { Service } from '../index'
import { CityId, Route } from '../services/path-finder/path-finder'

interface RoutesCheapestRequestQuery {
  start?: CityId
  end?: CityId
}

interface RoutesCheapestResponseData {
  route: Route
}

type RoutesCheapestRequest = FastifyRequest<
  http.IncomingMessage,
  RoutesCheapestRequestQuery,
  unknown,
  unknown,
  unknown
>

const schema: RouteSchema = {
  querystring: {
    type: 'object',
    required: [
      'start', //
      'end',
    ],
    properties: {
      start: {
        type: 'string',
        minLength: 1,
      },
      end: {
        type: 'string',
        minLength: 1,
      },
    },
  },
  response: {
    '2xx': {
      type: 'object',
      required: ['data'],
      properties: {
        data: {
          type: 'object',
          properties: {
            route: {
              $ref: 'path-finder.common.json#route'
            }
          }
        }
      }
    },
  },
}

async function handler(this: FastifyInstance, request: RoutesCheapestRequest): Promise<JSONSchemaResponse<RoutesCheapestResponseData>> {
  const { query } = request

  try {
    const route = (this as Service).finder.cheapest(query?.start, query?.end)
    return {
      data: {
        route
      }
    }
  } catch (err) {
    throw this.httpErrors.notFound('No Such Route')
  }
}

const routesCheapest: RouteOptions = {
  handler,
  schema,
  method: 'GET',
  url: '/routes/cheapest',
}

export = routesCheapest
