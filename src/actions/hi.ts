import { FastifyInstance, RouteOptions, RouteSchema } from 'fastify'

const schema: RouteSchema = {
  body: {
    default: null,
  },
  response: {
    '2xx': {
      type: 'string',
    },
  },
}

async function handler(this: FastifyInstance): Promise<string> {
  return 'Hi'
}

const hi: RouteOptions = {
  handler,
  schema,
  method: 'GET',
  url: '/hi',
}

export = hi
