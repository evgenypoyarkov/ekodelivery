import * as http from 'http'
import { FastifyInstance, FastifyRequest, RouteOptions, RouteSchema } from 'fastify'
import { Service } from '../index'
import { Route } from '../services/path-finder/path-finder'

interface RoutesCostRequestQuery {
  route?: string[]
}

interface RoutesCostResponseData {
  route: Route
}

type RoutesCostRequest = FastifyRequest<
  http.IncomingMessage,
  RoutesCostRequestQuery,
  unknown,
  unknown,
  unknown
>

const schema: RouteSchema = {
  querystring: {
    type: 'object',
    required: [
      'route', //
    ],
    properties: {
      route: {
        type: 'array',
        items: {
          $ref: '#/definitions/step',
        },
        minItems: 2,
      }
    },
    definitions: {
      step: {
        type: 'string',
        pattern: '^\\w$'
      }
    }
  },
  response: {
    '2xx': {
      type: 'object',
      required: ['data'],
      properties: {
        data: {
          type: 'object',
          properties: {
            route: {
              $ref: 'path-finder.common.json#route'
            }
          }
        }
      }
    },
  },
}

async function handler(this: FastifyInstance, request: RoutesCostRequest): Promise<JSONSchemaResponse<RoutesCostResponseData>> {
  const { query } = request

  try {
    const route = (this as Service).finder.cost(query?.route)
    return {
      data: {
        route
      }
    }
  } catch (err) {
    throw this.httpErrors.notFound('No Such Route')
  }
}

const routesCost: RouteOptions = {
  handler,
  schema,
  method: 'GET',
  url: '/routes/cost',
}

export = routesCost
