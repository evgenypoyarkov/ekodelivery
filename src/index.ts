import path = require('path')
import pino = require('pino')
import merge = require('lodash/merge')
import Fastify = require('fastify')
import cors = require('fastify-cors')
import Sensible = require('fastify-sensible')
import shutdown = require('fastify-graceful-shutdown')
import registerRoutes = require('fastify-register-routes')

import pathFinder = require('./services/path-finder')

import { conf } from './config'
import { PathFinder, PathFinderConfig } from './services/path-finder/path-finder'
import { onShutdown } from './utils/on-shutdown'

const routesPath = path.resolve(__dirname, './actions')
const log = pino()

export interface ServiceConf {
  gracefulShutdown: boolean
  server: Fastify.ListenOptions
  fastify: Fastify.ServerOptions
  cors: cors.Options
  pathFinder: PathFinderConfig
}

export interface Service extends Fastify.FastifyInstance {
  conf: ServiceConf
  finder: PathFinder
}

export async function main(opts?: DeepPartial<ServiceConf>): Promise<Service> {
  const config: ServiceConf = merge({}, conf.get('/'), opts)
  const fastify = Fastify(config.fastify)

  fastify.decorate('conf', config)

  fastify.register(Sensible)
  fastify.register(pathFinder, config.pathFinder)
  fastify.register(cors, config.cors)
  fastify.register(registerRoutes, {
    path: routesPath,
    regex: /.*\.js/,
    showTable: false,
  })

  if (config.gracefulShutdown) {
    fastify.register(shutdown).after(onShutdown)
  }

  await fastify.ready()
  await fastify.listen(config.server)

  return fastify as Service
}

if (module.parent === null) {
  main()
    .then(() => log.info('launched!'))
}
