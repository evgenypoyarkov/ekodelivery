import test from 'ava'
import { Service } from '../../src'
import { init, close, TestContext } from '../helpers/common'

test.before(init)
test.after(close)

test('greets', async (t: TestContext) => {
  const service = t.context.service as Service
  const res = await service.inject({
    url: '/hi',
    method: 'GET',
  })

  t.true(res.statusCode === 200)
  t.true(res.payload === 'Hi')
  t.pass()
})
