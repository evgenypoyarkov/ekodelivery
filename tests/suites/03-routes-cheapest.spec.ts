import test from 'ava'
import { Route } from '../../src/services/path-finder/path-finder'
import { init, close, load, TestContext } from '../helpers/common'

test.before(init)
test.before(load)
test.after(close)

test('calculates the cheapest route', async (t: TestContext) => {
  const { service } = t.context
  const ee = service?.finder.cheapest('E', 'E') as Route
  t.true(ee.cost === 6)

  const ed = service?.finder.cheapest('E', 'D') as Route
  t.true(ed.cost === 9)
})

test('serves the HTTP endpoint', async (t: TestContext) => {
  const { service } = t.context
  const res = await service?.inject({
    url: '/routes/cheapest',
    query: {
      start: 'E',
      end: 'E',
    },
  })

  const payload = res?.payload ? JSON.parse(res?.payload) : {}

  t.log(res?.payload)
  t.true(payload.data.route.cost === 6)
  t.pass()
})

