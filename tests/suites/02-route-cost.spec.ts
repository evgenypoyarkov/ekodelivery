import test from 'ava'
import { kBadRequest, kNotFound } from '../../src/services/path-finder/constants'
import { Route } from '../../src/services/path-finder/path-finder'
import { init, close, load, TestContext } from '../helpers/common'

test.before(init)
test.before(load)
test.after(close)

test('calculates the delivery cost of various routes', async (t: TestContext) => {
  const { service } = t.context
  const abe = service?.finder.cost([
    'A',
    'B',
    'E',
  ]) as Route
  t.true(abe.cost === 4)

  const ad = service?.finder.cost([
    'A',
    'D',
  ]) as Route
  t.true(ad.cost === 10)

  const eacf = service?.finder.cost([
    'E',
    'A',
    'C',
    'F',
  ]) as Route
  t.true(eacf.cost === 8)
})

test('throws if route is too short', async (t: TestContext) => {
  try {
    const { service } = t.context
    const a = service?.finder.cost([
      'A',
    ]) as Route
    t.log(a)
    t.fail()
  } catch (err) {
    t.true(err === kBadRequest)
    t.pass()
  }
})

test('throws if route consists only of instances of the same city', async (t: TestContext) => {
  try {
    const { service } = t.context
    const aaba = service?.finder.cost([
      'A',
      'A',
    ]) as Route
    t.log(aaba)
    t.fail()
  } catch (err) {
    t.true(err === kNotFound)
    t.pass()
  }
})

test('throws if route is not found', async (t: TestContext) => {
  try {
    const { service } = t.context
    const adf = service?.finder.cost([
      'A',
      'D',
      'F',
    ]) as Route
    t.log(adf)
    t.fail()
  } catch (err) {
    t.true(err === kNotFound)
    t.pass()
  }
})

test('operates via HTTP', async (t: TestContext) => {
  const res = await t.context.service?.inject({
    url: '/routes/cost',
    query: {
      route: [
        'A',
        'B',
        'E',
        'B'
      ]
    }
  })

  const payload = res?.payload ? JSON.parse(res?.payload) : {}

  t.log(res?.payload)
  t.true(payload.data.route.cost === 7)
  t.true(res?.statusCode === 200)
  t.pass()
})

test('returns 400 if less then 2 cities provided', async (t: TestContext) => {
  const res = await t.context.service?.inject({
    url: '/routes/cost',
    query: {
      route: [
        'A',
      ]
    }
  })

  t.true(res?.statusCode === 400)
  t.pass()
})

test('returns 404 if the route not found', async (t: TestContext) => {
  const res = await t.context.service?.inject({
    url: '/routes/cost',
    query: {
      route: [
        'B',
        'A',
      ]
    }
  })

  t.true(res?.statusCode === 404)
  t.pass()
})
