import qs from 'qs'
import test from 'ava'
import { init, close, load, TestContext } from '../helpers/common'

test.before(init)
test.before(load)
test.after(close)

test('count all possible routes w/o using the same route twice with max 4 stops', async (t: TestContext) => {
  const { service } = t.context
  const ed = service?.finder.count('E', 'D', {
    maxStops: 4,
    useTwice: false,
  })

  t.true(ed === 4)
  t.pass()
})

test('count all possible routes w/o using the same route twice', async (t: TestContext) => {
  const { service } = t.context
  const ee = service?.finder.count('E', 'E', {
    useTwice: false,
  })

  // I'm not sure if my answer is correct since it doesn't match the expected according the task spec.
  // I've double checked it manually, but each time I got the same result as my algo.
  // Same for the next case.
  //
  // EBEADE 4
  // EBEACDE 5
  // EBEACFDE 6
  // EADE 2
  // EABE 2
  // EBE 1
  // EACFDE 4
  // EACDE 3
  t.true(ee === 8)
  t.pass()
})

test('count all possible routes limited by their length, allow use the route twice', async (t: TestContext) => {
  const { service } = t.context
  const ee = service?.finder.count('E', 'E', {
    useTwice: true,
    maxLength: 20,
  })

  t.true(ee === 25)
  t.pass()
})

test('serves HTTP endpoint', async (t: TestContext) => {
  const { service } = t.context
  const req = qs.stringify({
    start: 'E',
    end: 'A',
    conditions: {
      useTwice: true,
      maxStops: 1
    }
  }, {
    allowDots: true,
  })
  const res = await service?.inject(`/routes/count?${req}`)
  const payload = res?.payload ? JSON.parse(res?.payload) : {}

  t.log(res?.payload)
  t.true(payload.data.count === 1) // e -> a
  t.pass()
})
