import test from 'ava'
import { init, close, TestContext } from '../helpers/common'

test.before(init)
test.after(close)

test('no roads after initialization', async (t: TestContext) => {
  const { service } = t.context
  const roads = service?.finder.list()

  t.true(roads?.length === 0)
  t.pass()
})

test('adds roads directly via the path-finder service', async (t: TestContext) => {
  const { service } = t.context
  const res = service?.finder.add([{
    start: 'A',
    end: 'B',
    distance: 1,
  }, {
    start: 'B',
    end: 'C',
    distance: 2,
  }])

  // is added
  t.true(res === 2)

  const roads = service?.finder.list()
  t.true(roads?.length === res)
  t.pass()
})

test('it is possible to submit routes via HTTP API', async (t: TestContext) => {
  const { service } = t.context
  const res = await service?.inject({
    url: '/roads/add',
    method: 'POST',
    payload: {
      roads: [
        'AD5',
        'CE6',
        'CD10',
      ]
    }
  })

  const payload = res?.payload ? JSON.parse(res?.payload) : {}
  t.true(res?.statusCode === 200)
  t.true(payload.data?.added === 3)
  t.pass()
})

test('updates the distance between points', async (t: TestContext) => {
  const { service } = t.context
  const before = service?.finder.list()
  const res = await service?.inject({
    url: '/roads/add',
    method: 'POST',
    payload: {
      roads: [
        'AD10',
      ]
    }
  })

  const after = service?.finder.list()
  const payload = res?.payload ? JSON.parse(res?.payload) : {}

  t.true(res?.statusCode === 200)
  t.true(payload.data?.added === 0)
  t.true(before?.length === after?.length)
  t.pass()
})

test('rejects to accept malformed roads - w/o distance', async (t: TestContext) => {
  const { service } = t.context
  const res = await service?.inject({
    url: '/roads/add',
    method: 'POST',
    payload: {
      roads: [
        'AD',
      ]
    }
  })

  t.true(res?.statusCode === 400)
  t.pass()
})

test('rejects to accept malformed roads - word-num-num', async (t: TestContext) => {
  const { service } = t.context
  const res = await service?.inject({
    url: '/roads/add',
    method: 'POST',
    payload: {
      roads: [
        'A10',
      ]
    }
  })

  t.true(res?.statusCode === 400)
  t.pass()
})

test('rejects to accept malformed roads - num-word-num', async (t: TestContext) => {
  const { service } = t.context
  const res = await service?.inject({
    url: '/roads/add',
    method: 'POST',
    payload: {
      roads: [
        '1B0',
      ]
    }
  })

  t.true(res?.statusCode === 400)
  t.pass()
})

test('rejects to accept the same route twice within the same query', async (t: TestContext) => {
  const { service } = t.context
  const res = await service?.inject({
    url: '/roads/add',
    method: 'POST',
    payload: {
      roads: [
        'AD5',
        'AD5',
      ]
    }
  })

  t.true(res?.statusCode === 400)
  t.pass()
})
