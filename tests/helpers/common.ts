import { ExecutionContext } from 'ava'
import { main, Service } from '../../src'
import { roads } from '../fixtures/roads'

export type TestContext = ExecutionContext<{
  service?: Service
  data?: any
}>

export async function init(t: TestContext): Promise<void> {
  t.context.service = await main({
    /** options */
  })

  t.context.data = {}
}

export async function close(t: TestContext): Promise<void> {
  const { service } = t.context

  if (service) {
    await service.close()
    t.context.service = undefined
    t.context.data = undefined
  }
}

export async function load(t: TestContext): Promise<void> {
  const res = await t.context.service?.inject({
    url: '/roads/add',
    method: 'POST',
    payload: {
      roads,
    }
  })

  t.log(t.context.service)
  t.log(res)
  t.true(res?.statusCode === 200)
  t.true((JSON.parse(res?.payload as string) as any).data.added === roads.length)
}
